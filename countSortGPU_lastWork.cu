#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <Windows.h>

#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#define CUDA_ERROR(call) {                                        \
  cudaError err = call;                                           \
  if(err != cudaSuccess) {                                        \
    fprintf(stderr, "CUDA error in file '%s' in line %i: %s.\n",  \
    __FILE__, __LINE__, cudaGetErrorString(err));                 \
    exit(1);                                                      \
    }                                                             \
} while (0)

#define MAX_VALUE_INTO_ARRAY 1 << 24

#define uint unsigned int


#define CUDA_WRITE_ARRAY(tread, block, arrayValue, type, str, startIndex, finishIndex){	\
		__syncthreads();																\
		if (threadIdx.x == tread && blockIdx.x == block)								\
		{																				\
			printf("Array cuda debug. %s\n", str);										\
			for (uint i = startIndex; i < finishIndex; ++i)								\
			{																			\
				printf(type, arrayValue[i]);											\
			}																			\
			printf("\n");																\
		}																				\
		__syncthreads();																\
		}while(0)

#define CUDA_WRITE_VALUE(str, ARGUMENT_PRINTF){							 \
		__syncthreads();												 \
		if (threadIdx.x == 0 && blockIdx.x == 0)						 \
		{																 \
			printf("Array cuda debug. %s\n", str);						 \
			printf(ARGUMENT_PRINTF);									 \
		}																 \
		__syncthreads();												 \
		}while (0)

#define CUDA_COPY_TO_CPU_WRITE_ARRAY(count, GPU){									\
		uint *CPU____TEMP = (uint *) malloc(sizeof(uint) * count);					\
		cudaMemcpy(CPU____TEMP, GPU, sizeof(uint) * count, cudaMemcpyDeviceToHost);	\
		for (uint i = 0; i < count; ++i)											\
		{																			\
			std::cout << CPU____TEMP[i] << " ";										\
		}																			\
		std::cout << std::endl;														\
		free(CPU____TEMP);															\
		}while(0)

#define CPU_WRITE_ARRAY(count, CPU____TEMP){										\
		for (uint i = 0; i < count; ++i)											\
		{																			\
			std::cout << (CPU____TEMP)[i] << " ";									\
		}																			\
		std::cout << std::endl;														\
		}while(0)

#define CUDA_MAX_THREADS 256
#define CUDA_SUBARRAYS_PER_BLOCK 1
#define CUDA_MIN_ARRAY_FOR_GPU 50000
#define CUDA_MAX_BLOCKS 16
#define CUDA_MAX_SHARED_MEMORY CUDA_MAX_THREADS << 1
#define CUDA_MAX_GPU_SHARED_MEMORY 12288
#define CUDA_MIN_PINNED_MEMORY_SIZE CUDA_MIN_ARRAY_FOR_GPU
#define LOG_NUM_BANKS 5

#define CONFLICT_FREE_OFFS(i) ((i) >> LOG_NUM_BANKS)

/*DEVICE*/

__global__ void BuildTree_GPU(uint count, uint *vec, uint *treeArray, uint *finalElementsPerBlocks)
{
	__shared__ uint cache[CUDA_MAX_SHARED_MEMORY];
	uint tid = threadIdx.x;
	uint block_size = blockDim.x << 1;
	uint shift = block_size * blockIdx.x; // ����� ������� ������� � 2 ���� �������. ����� �� ��������� � shared_memory ������ � 2 ���� ������ ����� �������
	// ��� ��������
	uint reqursion_step = 0;
	uint count_threads = blockDim.x;
	uint counter = 1;
	uint first_value = tid << 1;
	uint second_value = first_value + 1;

	uint block_sizeGridDim = block_size * gridDim.x;
	uint local_shift = 0; 
	uint index_a = 0;
	uint index_b = 0;

	// ��� �����������
	uint blockDimWithoutOne = blockDim.x - 1;
	uint firstValuePlusOne = first_value + 1;
	uint secondValuePlusOne = second_value + 1;
	uint localShiftPlusFirstValue = 0;
	uint localShiftPlusSecondValue = 0;

	while (reqursion_step < count)
	// ��������
	{
		local_shift = shift + reqursion_step * block_sizeGridDim;
		count_threads = blockDim.x;
		counter = 1;
		localShiftPlusFirstValue = first_value + local_shift;
		localShiftPlusSecondValue = second_value + local_shift;
		// �������� ������ � ����������� ������
		/*shift -- ����� ������ �����*/
		/*block_step * block_size * gridDim.x -- ����� ��������*/
		cache[first_value] = vec[localShiftPlusFirstValue];
		cache[second_value] = vec[localShiftPlusSecondValue];

		if (tid == blockDimWithoutOne)
		// ���������� ��������� ������� ��� ����, ����� ����������� �����
		{
			// �����������. ������
			// finalElementsPerBlocks[blockIdx.x + reqursion_step * gridDim.x] = vec[second_value + local_shift];
			finalElementsPerBlocks[blockIdx.x + reqursion_step * gridDim.x] = cache[second_value];
		}

		__syncthreads();

		while (count_threads > 0)
			// ����������� �� ������
		{

			if (tid < count_threads)
				// ��������, ����� ������ �������� ��� ������������ �� ������
			{
				index_a = counter * firstValuePlusOne - 1;
				index_b = counter * secondValuePlusOne - 1;
				cache[index_b] += cache[index_a];
			}
			__syncthreads();

			count_threads >>= 1;
			counter <<= 1;
		}
		// ������������ �� �������, � ���������� ������
		treeArray[localShiftPlusFirstValue] = cache[first_value];
		treeArray[localShiftPlusSecondValue] = cache[second_value];

		++reqursion_step;
	}

	return;
}

__global__ void BuildExcludeSubPreffixSum_GPU(uint count, uint *tree, uint *subExcludePreffixSum)
{
	__shared__ uint cache[CUDA_MAX_SHARED_MEMORY];
	// ������ ����������� �� �������. ����� ����
	// __shared__ uint count_shared[1];
	// count_shared[0] = count;
	uint tid = threadIdx.x;
	uint block_size = blockDim.x << 1;
	uint shift = block_size * blockIdx.x; // ����� ������� ������� � 2 ���� �������. ����� �� ��������� � shared_memory ������ � 2 ���� ������ ����� �������
	// ��� ��������
	uint reqursion_step = 0;
	uint count_threads = blockDim.x;
	uint counter = 1;
	uint first_value = tid << 1;
	uint second_value = first_value + 1;
	uint block_sizeGridDim = block_size * gridDim.x;
	uint index_a = 0;
	uint index_b = 0;
	uint temp = 0;
	uint local_shift = 0;

	// ��� �����������
	uint blockDimWithoutOne = blockDim.x - 1;
	uint blockSizeWithoutOne = block_size - 1;
	uint firstValuePlusOne = second_value;
	uint secondValuePlusOne = second_value + 1;
	uint localShiftPlusSecondValue = 0;
	uint localShiftPlusFirstValue = 0;

	while (reqursion_step < count)
	// ��������
	{
		count_threads = 1;
		counter = blockDim.x;

		// �������� ������ � ����������� ������
		/*shift -- ����� ������ �����*/
		/*block_step * block_size * gridDim.x -- ����� ��������*/
		local_shift = shift + reqursion_step * block_sizeGridDim;
		localShiftPlusFirstValue = first_value + local_shift;
		localShiftPlusSecondValue = second_value + local_shift;

		cache[first_value] = tree[localShiftPlusFirstValue];
		cache[second_value] = tree[localShiftPlusSecondValue];

		if (tid == blockDimWithoutOne)
		// ������ ���, ����� ������� ��������� ������� ������ ���, �����, ������� �������� �� ������ � ���� ��������� �� ������ ��������, ����� �� ����������� ��������
		{
			cache[blockSizeWithoutOne] = 0;
		}

		__syncthreads();

		while (count_threads <= blockDim.x)
		// ����������� �� ������
		{

			if (tid < count_threads)
				// ��������, ����� ������ �������� ��� ������������ �� ������
			{
				index_a = counter * firstValuePlusOne - 1;
				index_b = counter * secondValuePlusOne - 1;
				temp = cache[index_a];

				cache[index_a] = cache[index_b];
				cache[index_b] += temp;
			}

			__syncthreads();

			count_threads <<= 1;
			counter >>= 1;
		}
		// ������������ �� �������, � ���������� ������


		subExcludePreffixSum[localShiftPlusFirstValue] = cache[first_value];
		subExcludePreffixSum[localShiftPlusSecondValue] = cache[second_value];

		++reqursion_step;
	}

	return;
}

__global__ void BuildIncludeSubPreffixSumByExcludeSubPreffixSum_GPU(uint count, uint *subExcludePreffixSum, uint *subIncludePreffixSum, uint *finalElementsPerBlocks, uint *subSumsPerBlocks)
{
	__shared__ uint cache[CUDA_MAX_SHARED_MEMORY];
	// ������ ����������� �� �������. ����� ����
	// __shared__ uint count_shared[1];
	// count_shared[0] = count;
	uint tid = threadIdx.x;
	uint block_size = blockDim.x << 1;
	uint shift = block_size * blockIdx.x; // ����� ������� ������� � 2 ���� �������. ����� �� ��������� � shared_memory ������ � 2 ���� ������ ����� �������
	// ��� ��������
	uint reqursion_step = 0;
	uint first_value = tid << 1;
	uint second_value = first_value + 1;
	uint block_sizeGridDim = block_size * gridDim.x;

	// �����������
	uint blockSizeWithoutOne = block_size - 1;
	uint blockDimWithoutOne = blockDim.x - 1;

	while (reqursion_step < count)
	// ��������
	{
		/*shift -- ����� ������ �����*/
		/*block_step * block_size * gridDim.x -- ����� ��������*/
		uint local_shift = shift + reqursion_step * block_sizeGridDim;
		uint index_a = first_value + local_shift;
		uint index_b = second_value + local_shift;
		uint shift_block = blockIdx.x + reqursion_step * gridDim.x;


		// �������� ������ � ����������� ������ �� �������
		cache[first_value] = subExcludePreffixSum[index_b];
		if (second_value == blockSizeWithoutOne)
		{
			// �����������
			// cache[second_value] = subExcludePreffixSum[index_a + 1] + finalElementsPerBlocks[shift_block];
			cache[second_value] = cache[first_value] + finalElementsPerBlocks[shift_block];
		}
		else
		{
			cache[second_value] = subExcludePreffixSum[index_b + 1];
		}
		if (tid == blockDimWithoutOne)
		// ���������� ��������� �����
		{
			// �����������
			// subSumsPerBlocks[shift_block] = subExcludePreffixSum[index_b] + finalElementsPerBlocks[shift_block];
			subSumsPerBlocks[shift_block] = cache[first_value] + finalElementsPerBlocks[shift_block];
		}
		
		// ������� �������� ������������� �������, ����� �� ���� ����������
		__syncthreads();

		// �������� ������ ������� 
		subIncludePreffixSum[index_a] = cache[first_value];
		subIncludePreffixSum[index_b] = cache[second_value];

		++reqursion_step;

		// ���, ���� �� �����������, ������ ��� ������ ����� ���� �����������, ����� �� ���� ��������� ������
		// __syncthreads(); �������� ������!!!!!!!!!!!!!!!!!!!!!
	}


	return;
}


__global__ void BuildIncludePreffixSumByIncludeSubPreffixSum_GPU(uint count, uint *subIncludeSums, uint *includeSums, uint *subSums)
{
	// ������ ����������� �� �������. ����� ����
	// __shared__ uint count_shared[1];
	// count_shared[0] = count;
	uint tid = threadIdx.x + (blockIdx.x + 1) * blockDim.x;
	uint bid = blockIdx.x;
	uint step_threads = gridDim.x * blockDim.x;
	uint step_blocks = gridDim.x;
	uint index = tid << 1;

	while (index + 1 < count)
	{
		includeSums[index] = subIncludeSums[index] + subSums[bid];
		includeSums[index + 1] = subIncludeSums[index + 1] + subSums[bid];

		tid += step_threads;
		bid += step_blocks;

		index = tid << 1;
	}
	return;
}

__global__ void Counting_GPU_WithoutShared(uint count, uint *vec, uint *countingArray)
{
	uint tid = threadIdx.x + blockDim.x * blockIdx.x;
	uint delta = blockDim.x * gridDim.x;
	for (uint i = tid; i < count; i += delta)
	{
		atomicAdd(&(countingArray[vec[i]]), 1);
	}
	return;
}


__global__ void Counting_GPU_WithShared(uint count, uint *vec, uint *countingArray, uint countShared)
{
	__shared__ uint local_countingArray[CUDA_MAX_SHARED_MEMORY];

	local_countingArray[threadIdx.x] = 0;

	__syncthreads();

	uint bid = blockIdx.x;
	uint tid = threadIdx.x + blockDim.x * blockIdx.x;
	uint delta = blockDim.x * gridDim.x;

	for (uint i = tid; i < count; i += delta)
	{
		if (countShared * bid <= vec[i] && vec[i] < countShared * (bid + 1) )
		{
			atomicAdd(&(local_countingArray[vec[i] ]), 1);
		}
	}

	__syncthreads();

	uint shift = countShared * bid;
	for (uint i = threadIdx.x; i < countShared; i += blockDim.x)
	{
		atomicAdd(&vec[shift + i], local_countingArray[i]);
	}

	return;
}

/*HOST*/

__host__ float FinishTime(cudaEvent_t *start, cudaEvent_t *stop)
{
	float t = 0;
	cudaEventRecord(*stop, 0);
	cudaEventSynchronize(*stop);
	cudaEventElapsedTime(&t, *start, *stop);
	cudaEventDestroy(*start);
	cudaEventDestroy(*stop);
	return t;
}

__host__ void StartTime(cudaEvent_t *start, cudaEvent_t *stop)
{
	cudaEventCreate(start);
	cudaEventCreate(stop);
	cudaEventRecord(*start, 0);
	return;
}

__host__ void Counting_CPU(uint count, uint *vec, uint *countingArray)
{
	for (uint i = 0; i < count; ++i)
	{
		++countingArray[vec[i]];
	}
	return;
}

__host__ void Answer_CPU(uint count, uint *vec, uint *countingArray)
{
	uint index = 0;
	for (uint i = 0; i < count; ++i)
	{
		for (uint j = 0; j < countingArray[i]; ++j)
		{
			vec[index] = i;
			++index;
		}
	}
	return;
}

__host__ void PreffixSum_CPU(uint count, uint *input, uint *output)
{
	output[0] = input[0];
	for (uint i = 1; i < count; ++i)
	{
		output[i] = input[i] + output[i - 1];
	}
	return;
}



__host__ uint GetThreads(uint count)
{
	uint threads = 0;
	if (count > CUDA_MIN_ARRAY_FOR_GPU)
	{
		threads = CUDA_MAX_THREADS;
	}
	return threads;
}

__host__ uint GetBlocks(uint count)
{
	uint blocks = 0;
	uint threads = GetThreads(count);
	if (count > CUDA_MIN_ARRAY_FOR_GPU)
	{
		blocks = count / (CUDA_SUBARRAYS_PER_BLOCK * (2 * threads) );
		if (count % (CUDA_SUBARRAYS_PER_BLOCK * (2 * threads)) != 0)
		{
			blocks += 1;
		}
		if (blocks > CUDA_MAX_BLOCKS)
		{
			blocks = CUDA_MAX_BLOCKS;
		}
		return blocks;
	}
	else
	{
		return 0;
	}
}

void MallocHost(uint **data, uint count)
{
	uint threads = GetThreads(count);
	uint blocks = GetBlocks(count);
	uint newCount = count;
	uint gridDim = 2 * threads * blocks;
	if (gridDim != 0 && count % gridDim != 0)
	{
		newCount += gridDim - (count % gridDim);
	}

	if (gridDim == 0 && newCount < CUDA_MIN_PINNED_MEMORY_SIZE)
	{
		*data = (uint *) malloc(sizeof(uint) * newCount);
	}
	else
	{
		// ���������� pinned-memory
		cudaHostAlloc(data, sizeof(uint) * newCount, cudaHostAllocDefault);
	}
	return;
}

void FreeHost(uint *data, uint count)
{
	uint threads = GetThreads(count);
	uint blocks = GetBlocks(count);
	uint newCount = count;
	uint gridDim = 2 * threads * blocks;
	if (gridDim != 0 && count % gridDim != 0)
	{
		newCount += gridDim - (count % gridDim);
	}

	if (gridDim == 0 || newCount < CUDA_MIN_PINNED_MEMORY_SIZE)
	{
		free(data);
	}
	else
	{
		// ���������� pinned-memory
		cudaFreeHost(data);
	}
}

__host__ void Scan__FINAL_ELEMENTS(uint count, uint *dev_input, uint *dev_output)
{
	uint *sums0_ = NULL;
	MallocHost(&sums0_, count);
	cudaMemcpyAsync(sums0_, dev_input, sizeof(uint) * count, cudaMemcpyDeviceToHost);
	PreffixSum_CPU(count, sums0_, sums0_);
	cudaMemcpyAsync(dev_output, sums0_, sizeof(uint) * count, cudaMemcpyHostToDevice);
	FreeHost(sums0_, count);
}

__host__ void Scan__GPU(uint threads, uint blocks, uint count, uint *dev_input, uint *dev_output)
{
	if (threads == 0)
	{
		Scan__FINAL_ELEMENTS(count, dev_input, dev_output);
		return;
	}
	uint newCount = count / (2 * threads);

	if (count % (2 * threads) != 0)
	{
		++newCount;
	}

	uint block_size = 0;
	uint max_iteration_reqursion = 0;

	uint newThreads = GetThreads(newCount);
	uint newBlocks = GetBlocks(newCount);


	uint *dev_tree = NULL;
	uint *dev_finalElementsPerBlocks = NULL;
	uint *dev_finalSumElementsPerBlocks = NULL;
	uint *dev_subExcludePreffixSums = NULL;
	uint *dev_subIncludePreffixSums = NULL;
	uint *dev_subSumsPerBlocks = NULL;

	uint cudaError = 0;

	dev_tree = dev_input;
	dev_subExcludePreffixSums = dev_input;
	dev_subIncludePreffixSums = dev_input;
	cudaError = cudaMalloc(&dev_finalElementsPerBlocks, sizeof(uint) * newCount); // ����� ��� �������������� ������. ����� �������� ���� ��� � ���������� � ����������.
	dev_subSumsPerBlocks = dev_finalElementsPerBlocks;
	dev_finalSumElementsPerBlocks = dev_finalElementsPerBlocks;

	if (!cudaError)
	{
		block_size = 2 * threads;
		max_iteration_reqursion = count / (block_size * blocks) + (((count - count / (block_size * blocks) * block_size * blocks) != 0) ? 1 : 0);
		BuildTree_GPU<<<blocks, threads>>>(max_iteration_reqursion, dev_input, dev_tree, dev_finalElementsPerBlocks);

		BuildExcludeSubPreffixSum_GPU<<<blocks, threads>>>(max_iteration_reqursion, dev_tree, dev_subExcludePreffixSums);
		BuildIncludeSubPreffixSumByExcludeSubPreffixSum_GPU<<<blocks, threads>>>(max_iteration_reqursion,
																				 dev_subExcludePreffixSums, dev_subIncludePreffixSums,
																				 dev_finalElementsPerBlocks,
																		    	 dev_subSumsPerBlocks);
		Scan__GPU(newThreads, newBlocks, newCount, dev_finalElementsPerBlocks, dev_finalSumElementsPerBlocks);
		BuildIncludePreffixSumByIncludeSubPreffixSum_GPU<<<blocks, threads>>>(count, dev_subIncludePreffixSums, dev_output, dev_finalElementsPerBlocks);
	}
	else
	{
		Scan__FINAL_ELEMENTS(count, dev_input, dev_output);
		return;
	}
	cudaFree(dev_finalElementsPerBlocks);
}


__host__ void Scan(uint count, uint **input, uint *output)
{
	if (count > CUDA_MIN_ARRAY_FOR_GPU)
	{
		cudaEvent_t start = NULL;
		cudaEvent_t stop = NULL;
		uint *dev_input = NULL;
		uint *dev_output = NULL;
		uint threads = GetThreads(count);
		uint blocks = GetBlocks(count); 
		uint newCount = count;
		uint gridDim = 2 * threads * blocks;
		if (count % gridDim != 0)
		{
			newCount += gridDim - (count % gridDim);
		}
		output = *input;
		uint *checkedArray = (uint *) malloc(sizeof(uint) * newCount);
		double startTime = clock();
		PreffixSum_CPU(newCount, *input, checkedArray);
		double finishTime = clock();
		std::cout << finishTime - startTime << std::endl;
		StartTime(&start, &stop);
		cudaMalloc(&dev_input, sizeof(uint) * newCount);
		cudaMemcpyAsync(dev_input, *input, sizeof(uint) * newCount, cudaMemcpyHostToDevice);
		dev_output = dev_input;
		Scan__GPU(threads, blocks, newCount, dev_input, dev_output);
		cudaMemcpyAsync(output, dev_output, sizeof(uint) * newCount, cudaMemcpyDeviceToHost);
		cudaFree(dev_input);
		std::cout << FinishTime(&start, &stop) << std::endl;

		for (uint i = 0; i < count; ++i)
		{
			if (checkedArray[i] != output[i])
			{
				std::cout << i << " ";
				std::cout << "Fail" << std::endl;
				break;
			}
		}
		free(checkedArray);
	}
	else
	{
		PreffixSum_CPU(count, *input, output);
	}
	return;
}

__host__ void CountSort_GPU(uint count, uint *vec, uint blocks, uint threads, uint sharedMemory)
{
}

__host__ void CountSort_CPU(uint count, uint *vec)
{
	uint *countingArray = NULL;

	countingArray = (uint *)malloc(sizeof(uint) * MAX_VALUE_INTO_ARRAY);

	memset(countingArray, 0, sizeof(uint) * MAX_VALUE_INTO_ARRAY);

	Counting_CPU(count, vec, countingArray);

	Answer_CPU(count, vec, countingArray);

	free(countingArray);
}

__host__ void ReadData(uint &count, uint **vec, std::string inName/* , uint &blocks, uint &threads*/)
{
	FILE *in = NULL;


	in = fopen(inName.c_str(), "rb");
	fread(&count, sizeof(uint), 1, in);

	MallocHost(vec, count);
	fread(*vec, sizeof(uint), count, in);
	fclose(in);
	return;
}


__host__ void WriteData(uint &count, uint *vec, std::string outName)
{
	FILE *out = NULL;
	out = fopen(outName.c_str(), "wb");
	fwrite(&count, sizeof(uint), 1, out);
	fwrite(vec, sizeof(uint), count, out);
	fclose(out);
	return;
}

__host__ int compare(const void *a_, const void *b_)
{
	uint *a = (uint *)a_;
	uint *b = (uint *)b_;

	if (*a < *b)
	{
		return -1;
	}
	if (*a > *b)
	{
		return 1;
	}
	return 0;
}

__host__ void QSort_CPU(uint count, uint *vec)
{
	qsort(vec, count, sizeof(uint), compare);
}

__host__ void GenerateData(uint count, uint maxValue, bool isPseudoRandom, std::string genName)
{
	FILE *out = NULL;
	out = fopen(genName.c_str(), "wb");
	fwrite(&count, sizeof(uint), 1, out);

	if (isPseudoRandom)
	{
		srand(time(NULL) );
	}

	for (int i = 0; i < count; ++i)
	{
		uint data = (rand() * rand() ) % maxValue;
		fwrite(&data, sizeof(uint), 1, out);
	}
	fclose(out);
}

int Check(std::string etalonName, std::string checkedName)
{
	uint checkedValue;
	uint etalonValue;
	uint countEtalon;
	FILE *inChecked;
	FILE *inEtalon;

	inEtalon = fopen(etalonName.c_str(), "rb");
	inChecked = fopen(checkedName.c_str(), "rb");

	fread(&countEtalon, sizeof(uint), 1, inChecked);
	fread(&countEtalon, sizeof(uint), 1, inEtalon);

	for (long int i = 0; i < countEtalon; ++i)
	{
		fread(&etalonValue, sizeof(uint), 1, inEtalon);
		fread(&checkedValue, sizeof(uint), 1, inChecked);
		if (checkedValue != etalonValue)
		{
			return 0;
		}
	}
	fclose(inChecked);
	fclose(inEtalon);
	return 1;
}

int main()
{
	/*DEVICE*/
	// ShareMemory == 6 ��

	/*TEST*/
	cudaEvent_t start = NULL;
	cudaEvent_t stop = NULL;
	cudaDeviceProp prop;
	float t = 0;

	cudaGetDeviceProperties(&prop, 0);

	std::cout << prop.name << " " << prop.warpSize << " " << prop.multiProcessorCount << " " << prop.warpSize << " " << std::endl;

	/*HOST*/

	std::string genName = "1.data";
	std::string inName = "1.data";
	std::string outNameCountSort_CPU = "CountSort_CPU.data";
	std::string outNameCountSort_GPU = "CountSort_GPU.data";
	std::string outNameQSort_CPU = "QSort_CPU.data";
	uint *vecCountSort_CPU = NULL;
	uint *vecCountSort_GPU = NULL;
	uint *vecQSort_CPU = NULL;
	uint count = 0;
	uint maxValue = 0;
	uint threads = 1;
	uint blocks = 3;
	uint sharedMemory = 1;
	bool isGPU = true;
	bool isPseudoRandom = false;

	// srand(time(NULL));
	std::cout << "Generated data" << std::endl;
	// std::cin >> count;
	for (uint i = 0; i < 1000; ++i)
	{
		count = 10000000; 
		count = count + (rand() * rand() ) % (2000000); 
		maxValue = 100;
		GenerateData(count, maxValue, isPseudoRandom, genName); //
		std::cout << "Step number: " << i << "; n = " << count << "; RAND_MAX = " << RAND_MAX << std::endl;


		// std::cout << "Enter input File\'s name:" << std::endl;
		// std::cin >> inName;
		// std::cin >> outNameCountSort_CPU

		std::cout << "Read data:" << std::endl;
		ReadData(count, &vecCountSort_GPU, inName);

		Scan(count, &vecCountSort_GPU, vecCountSort_GPU);
		FreeHost(vecCountSort_GPU, count);
	}
	return;

	vecCountSort_CPU = (uint *) malloc(sizeof(uint) * count);
	vecQSort_CPU = (uint *) malloc(sizeof(uint) * count);

	memcpy(vecCountSort_CPU, vecCountSort_GPU, count * sizeof(uint) );
	memcpy(vecQSort_CPU, vecCountSort_GPU, count * sizeof(uint) );

	std::cout << "Currently work CountSort on CPU:" << std::endl;
	StartTime(&start, &stop);
	CountSort_CPU(count, vecCountSort_CPU);
	std::cout << "Time work: " << FinishTime(&start, &stop) << std::endl;
	std::cout << "Time work: " << t << std::endl;
	std::cout << "Write data after Count Sort on CPU" << std::endl;
	WriteData(count, vecCountSort_CPU, outNameCountSort_CPU);

	if (isGPU)
	{
		std::cout << "Currently work CountSort on GPU:" << std::endl;
		std::cin >> blocks >> threads;
		std::cout << "Currently work CountSort on GPU:" << std::endl;
		StartTime(&start, &stop);
		CountSort_GPU(count, vecCountSort_GPU, blocks, threads, sharedMemory);
		std::cout << "Time work: " << FinishTime(&start, &stop) << std::endl;
		std::cout << "Write data after Count Sort on GPU" << std::endl;
		WriteData(count, vecQSort_CPU, outNameCountSort_GPU);
	}

	std::cout << "Currently work QuickSort:" << std::endl;
	StartTime(&start, &stop);
	QSort_CPU(count, vecQSort_CPU);
	std::cout << "Time work: " << FinishTime(&start, &stop) << std::endl;
	std::cout << "Write data after QuckSort" << std::endl;
	WriteData(count, vecQSort_CPU, outNameQSort_CPU);

	if (isGPU)
	{
		std::cout << "Count Sort CPU and Count Sort GPU: \n" << Check(outNameCountSort_CPU, outNameCountSort_GPU) << std::endl;
		std::cout << "Quick Sort CPU and Count Sort GPU: \n" << Check(outNameQSort_CPU, outNameCountSort_GPU) << std::endl;
	}
	std::cout << "Count Sort CPU and Count Sort CPU: \n" << Check(outNameQSort_CPU, outNameCountSort_CPU) << std::endl;

	free(vecQSort_CPU);
	free(vecCountSort_GPU);
	free(vecCountSort_CPU);

    return 0;
}